package com;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import java.util.concurrent.TimeUnit;

public class Main {

    public static void main(String args[]) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\raynu\\Desktop\\Учеба\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.get("http://frontend.nspk-aws-silver.luxoft.com/tomsk/register/");

        WebElement element = driver.findElement(By.xpath("//button/span[text()='Регистрация']"));
        element.click();

        WebElement phoneField = driver.findElement(By.xpath("//input[@class='input-block__field input input--rect']"));
        phoneField.click();
        phoneField.sendKeys("9999999998");
        Rectangle rect = driver.findElement(By.xpath("//label[@for='approve1']")).getRect();
        Actions actions = new Actions(driver);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].click();", driver.findElement(By.xpath("//label[@for='approve1']//span[@class='checkbox__bg']")));
        js.executeScript("arguments[0].click();", driver.findElement(By.xpath("//label[@for='approve2']//span[@class='checkbox__bg']")));
        driver.findElement(By.xpath("//button/span[text()='Продолжить']")).click();

        //WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
        //WebElement codeField = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='code']")));
        TimeUnit.SECONDS.sleep(3);
        WebElement codeField = driver.findElement(By.xpath("//input[@name='code']"));
        codeField.click();
        codeField.sendKeys("1111");

        driver.findElement(By.xpath("//button/span[text()='Продолжить']")).click();
        //TimeUnit.SECONDS.sleep(60);

    }
}
